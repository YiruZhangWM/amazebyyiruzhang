package edu.wm.cs.cs301.YiruZhang;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.appcompat.app.AppCompatActivity;

public class AMazeActivity extends AppCompatActivity {
    public static final String DRIVING_MODE = "driving_mode";
    public static final String DRIVER_KEY = "driver_key";
    public static final int RESULT_CODE = 0;
    public static final String EXPLORE_STATUS = "explore_status";
    public static final String DRIVER_MODE = "driver_mode";
    public static final String TITLE_STATUS = "title_status";
    public static final String EXPLORING_MODE = "exploring_mode";
    public static final String DIFF_LEVEL = "diff_level";
    private Button startButton;
    private TextView title1, title2, chooseDiffText, existingMazeText, chooseDriverText;
    private ToggleButton existingMazeOnOffButton;
    private Spinner existingMazeSpinner;
    private SeekBar seekBar;
    private RadioGroup radioGroup;
    private RadioButton manualButton, wallfollowerButton, wizardButton;

    public String driverMode, exploreMode;
    public int diffLevel = 1;   // default difficulty level is set to 1

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amaze);
        startButton = (Button)findViewById(R.id.startWelcome);
        title1 = (TextView) findViewById(R.id.title1);
        title2 = (TextView) findViewById(R.id.title2);
        chooseDiffText = (TextView) findViewById(R.id.chooseDiffText);
        existingMazeText = (TextView) findViewById(R.id.existingMaeText);
        chooseDriverText = (TextView) findViewById(R.id.driverChooseText);
        existingMazeOnOffButton = (ToggleButton) findViewById(R.id.existingMazeToggle);
        existingMazeSpinner = (Spinner) findViewById(R.id.existingMazeSpinner);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                chooseDiffText.setText(String.format("Choose a skill level: %s", i));
                diffLevel = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.v("diff_track_title", "State title: current difficulty level is: " + AMazeActivity.this.diffLevel);
            }
        });
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        manualButton = (RadioButton) findViewById(R.id.manualButton);
        wallfollowerButton = (RadioButton) findViewById(R.id.wallfollowerButton);
        wizardButton = (RadioButton) findViewById(R.id.wizardButton);

        // mode is default to manual
        driverMode = "manual";
        exploreMode = "explore";

        // deals with the spinner
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.existing_maze_spinner,
                android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        existingMazeSpinner.setAdapter(adapter);

        // deals with the toggle button to control explore/revisit mode
        existingMazeOnOffButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked){
                    exploreMode = "revisit";
                    Toast.makeText(AMazeActivity.this, "Revisiting an previous maze", Toast.LENGTH_SHORT).show();
                    Log.v(EXPLORE_STATUS, "State title: current exploreMode is: REVISIT");
                }
                else{
                    exploreMode = "explore";
                    Toast.makeText(AMazeActivity.this, "Exploring a brand-new maze", Toast.LENGTH_SHORT).show();
                    Log.v(EXPLORE_STATUS, "State title: current exploreMode is: EXPLORE");
                }
            }
        });
    }

    public void transitionToGen(View view) {
        //display a toast message
        displayToastMessage();

        //transition to state gen
        Intent intent = new Intent(this, GeneratingActivity.class);
        Log.v(TITLE_STATUS, "State title: transitionToGen, explore/revisit mode is: " + this.exploreMode);
        Log.v(TITLE_STATUS, "State title: transitionToGen, diff level is: " + this.diffLevel);
        Log.v(TITLE_STATUS, "State title: transitionToGen, driver mode is: " + this.driverMode);
        intent.putExtra(DRIVING_MODE, this.driverMode);
        intent.putExtra(EXPLORING_MODE, this.exploreMode);
        intent.putExtra(DIFF_LEVEL, this.diffLevel);
        startActivity(intent);
    }

    private void displayToastMessage(){
        String driverText = this.driverMode;
        Toast.makeText(this, String.format("You are in <%s> mode with difficulty <level %s> and driver mode <%s>",
                this.exploreMode, this.diffLevel, this.driverMode), Toast.LENGTH_LONG).show();
    }

    public void onRadioButtonClick(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch(view.getId()){
            case R.id.manualButton:
                if (checked){
                    Toast.makeText(this, "You choose to play manually", Toast.LENGTH_SHORT).show();
                    Log.v(DRIVER_MODE, "state title: manual mode is chosen");
                    this.driverMode = "manual";
                }
                break;
            case R.id.wallfollowerButton:
                if (checked){
                    Toast.makeText(this, "You choose the WallFollower as your driver", Toast.LENGTH_SHORT).show();
                    Log.v(DRIVER_MODE, "state title: wall follower mode is chosen");
                    this.driverMode = "wallfollower";
                }
                break;
            case R.id.wizardButton:
                if (checked){
                    Toast.makeText(this, "You choose the Wizard as your driver", Toast.LENGTH_SHORT).show();
                    Log.v(DRIVER_MODE, "state title: wizard mode is chosen");
                    this.driverMode = "wizard";
                }
                break;
        }
    }

    public void transitionToPlayForTest(View view) {
        Intent intent = new Intent(this, PlayManuallyActivity.class);
        startActivity(intent);
    }
}
