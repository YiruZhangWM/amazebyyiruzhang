package edu.wm.cs.cs301.YiruZhang;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class FinishActivity extends AppCompatActivity {
    public static final String RESTART_STATUS_FINAL = "restart_status_final";
    public static final String STATUS_REPORT = "status_report";
    Button restartButton;
    TextView congratText, distStatText, batteryStatText;
    ImageView congratImage;
    private int pathLen;
    private float batteryLeft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish);
        restartButton = (Button) findViewById(R.id.restartButtonFinal);
        congratText = (TextView) findViewById(R.id.congratText);
        distStatText = (TextView) findViewById(R.id.distStatText);
        batteryStatText = (TextView) findViewById(R.id.batteryStatText);
        congratImage = (ImageView) findViewById(R.id.congratsImage);

        // StateAnimationActivity sets the default pathLen to be 0 & batteryLeft to be 0.0
        // PlayManuallyActivity sets the default pathLen to be 2 & batteryLeft to be 2.0
        Intent intent = getIntent();
        if (intent.getIntExtra(StateAnimationActivity.PATH_LENGTH, -10) != -10){
            this.pathLen = intent.getIntExtra(StateAnimationActivity.PATH_LENGTH, -1);
            this.batteryLeft = intent.getFloatExtra(StateAnimationActivity.BATTERY_LEFT, -1f);
            Log.v(STATUS_REPORT, String.format("State final: get data from ANIMATION, pathLen is <%s> and batteryLeft is <%s>",
                    this.pathLen, this.batteryLeft));
        }
        else{
            this.pathLen = intent.getIntExtra(PlayManuallyActivity.PATH_LENGTH_MAN, -3);
            this.batteryLeft = intent.getFloatExtra(PlayManuallyActivity.BATTERY_LEFT_MAN, -3f);
            Log.v(STATUS_REPORT, String.format("State final: get data from MANUAL, pathLen is <%s> and batteryLeft is <%s>",
                    this.pathLen, this.batteryLeft));
        }

        distStatText.setText("Distance travelled: " + this.pathLen);
        batteryStatText.setText("Battery remaining: " + this.batteryLeft);
    }

    public void transitionToStartMenu(View view) {
        Log.v(RESTART_STATUS_FINAL, "State final: restart the title page");
        Intent intent = new Intent(this, AMazeActivity.class);
        startActivity(intent);
    }
}
