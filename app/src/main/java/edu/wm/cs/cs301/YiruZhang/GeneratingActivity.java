package edu.wm.cs.cs301.YiruZhang;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import edu.wm.cs.cs301.YiruZhang.generation.Factory;
import edu.wm.cs.cs301.YiruZhang.generation.MazeConfiguration;
import edu.wm.cs.cs301.YiruZhang.generation.MazeFactory;
import edu.wm.cs.cs301.YiruZhang.generation.Order;
import edu.wm.cs.cs301.YiruZhang.gui.MazePanel;


public class GeneratingActivity extends AppCompatActivity implements Order{
    public static final String DRIVER_STATUS = "driver_status";
    private Button backButton;
    private ProgressBar progressBar;
    private TextView genText;
    // mockProgressStatus, progressHandler deals with the progress bar
    private int mockProgressStatus = 0;
    // driving mode: manual, wallfollower, wizard
    // explore mode: explore (a new maze), revisit (a previous maze)
    private String driving_mode, explore_mode;
    // filename if maze is loaded from an existing maze
    private String filename;
    // determines the level of difficulty
    private int diff_level;
    private boolean stopThread = false;

    // for p5
    protected MazePanel panel;
    protected static MazeConfiguration config;
    protected static Factory factory;
    private Order.Builder builder;
    private int percentDone;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generating);

        // initialize variables for P5
        filename = null;
        factory = new MazeFactory();
        diff_level = 0;
        builder = Order.Builder.DFS;
        percentDone = 0;

        // gives reference to the views on screen
        backButton = (Button) findViewById(R.id.backButtonGen);
//        manualButton = (Button) findViewById(R.id.manualButtonGen);
//        animeButton = (Button) findViewById(R.id.animeButtonGen);
        progressBar = (ProgressBar) findViewById(R.id.determinateBar);
        genText = (TextView) findViewById(R.id.genText);

        // initialize progress bar thread and variables (for P4)
//        progressHandler = new Handler();
        final Intent intent = getIntent();
        driving_mode = intent.getStringExtra(AMazeActivity.DRIVING_MODE);
        explore_mode = intent.getStringExtra(AMazeActivity.EXPLORING_MODE);
        diff_level = intent.getIntExtra(AMazeActivity.DIFF_LEVEL, 1) - 1;

        Log.v("status_report", String.format("State gen: onCreate(), diff <%s>, exp <%s>, driver <%s>", this.diff_level, this.explore_mode
        , this.driving_mode));

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v("back_button_status", "State gen: back button pressed");
                percentDone = -1;
//                mockProgressStatus = -1;
                Toast.makeText(GeneratingActivity.this, "Back to start menu", Toast.LENGTH_SHORT).show();
//                transitionToStartPrivate();
                progressHandler.sendEmptyMessage(2);
            }
        });

        panel = new MazePanel(this);
        panel.setGenActivity(this);
        panel.genActivityStart();

        // 3 seconds to simulate progress bar precession
//        new Thread(runnable).start();

    }

//    Runnable runnable = new Runnable() {
//        @Override
//        public void run() {
//            while (mockProgressStatus < 100){
//                if (mockProgressStatus < 0){
//                    break;
//                }
//                mockProgressStatus++;
//                android.os.SystemClock.sleep(30);
//                progressHandler.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        progressBar.setProgress(mockProgressStatus);
//                    }
//                });
//            }
//            Log.v("progress_status", "State gen: progress bar is: " + mockProgressStatus);
//            if (mockProgressStatus >= 100) {
//                progressHandler.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        switch (driving_mode) {
//                            case "manual":
//                                Log.v(DRIVER_STATUS, "State gen (from Log.v()): manual activity generated");
//                                transitionToManualPrivate();
//                                break;
//                            case "wallfollower":
//                                Log.v(DRIVER_STATUS, "State gen (from Log.v()): wallfollower activity generated");
//                                transitionToAnimePrivate();
//                                break;
//                            case "wizard":
//                                Log.v(DRIVER_STATUS, "State gen (from Log.v()): wizard activity generated");
//                                transitionToAnimePrivate();
//                                break;
//                        }
//
//                    }
//                });
//            }
//        }
//    };

    // get methods for filname, diff_level, and builder
    public void setFileName(String filename) {
        this.filename = filename;
    }
    public void setSkillLevel(int skillLevel) {
        this.diff_level = skillLevel;
    }
    public void setBuilder(Order.Builder builder) {
        this.builder = builder;
    }
    @Override
    public int getSkillLevel() {
        return diff_level;
    }

    @Override
    public Builder getBuilder() {
        return builder;
    }

    // perfect is not implemented, so false is returned
    @Override
    public boolean isPerfect() {
        return false;
    }

    /**
     * FOR REVISIT MODE:
     * The deliver method is the call back method for the background
     * thread operated in the maze factory to deliver the ordered
     * product, here the generated maze in its container,
     * the MazeConfiguration object.
     */
    @Override
    public void deliver(MazeConfiguration mazeConfig) {
        this.config = mazeConfig;
        Log.v("gen_deliver","State gen: delivering");
//        progressHandler.sendEmptyMessage(1);
    }

    private Handler progressHandler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            int progress = GeneratingActivity.this.percentDone;
            switch (msg.what){
                case 0:
                    Log.v("progress_handler", String.format("State gen: msg = 0, set progress to: <%s>", progress));
                    progressBar.setProgress(progress);
                    break;
                case 1:
                    Log.v("progress_handler", "State gen: msg = 1, set progress to 100");
                    progressBar.setProgress(progress);
                    switch (GeneratingActivity.this.driving_mode){
                        case "manual":
                            Log.v(DRIVER_STATUS, "State gen: manual activity generated");
                            GeneratingActivity.this.transitionToManualPrivate();
                            break;
                        case "wallfollower":
                            Log.v(DRIVER_STATUS, "State gen: wallfollower activity generated");
                            GeneratingActivity.this.transitionToAnimePrivate();
                            break;
                        case "wizard":
                            Log.v(DRIVER_STATUS, "State gen: wizard activity generated");
                            GeneratingActivity.this.transitionToAnimePrivate();
                            break;
                        default:
                                Log.v("progress_handler",String.format("State gen: driving mode <%s> is not selected, error", GeneratingActivity.this.driving_mode));
                    }
                    break;
                case 2:
                    progressBar.setProgress(progress);
                    Log.v("progress_handler", "State gen: return to title menu");
                    GeneratingActivity.this.transitionToStartPrivate();
                    break;
            }
        }
    };

    @Override
    public void updateProgress(int percentage) {
        if (this.percentDone < percentage && percentage <= 100) {
            this.percentDone = percentage;

            // redraw the progress bar using a handler
            if (this.percentDone < 100) {
                Log.v("gen_update_progress",String.format("State gen: updating the progress by 1. Percentage: <%s>", percentage));
                progressHandler.sendEmptyMessage(0);
            }
            else if (this.percentDone >= 100){
                Log.v("gen_update_progress","State gen: update progress and send 1 to progress handler");
                progressHandler.sendEmptyMessage(1);
            }
        }
    }

    /**
     * Start the maze generation.
     * @param panel is the UI entity to produce the generating screen on
     */
    public void start(MazePanel panel) {
        if (panel == null){
            Log.v("gen_start","State gen: WARNING, start() has panel == null");
        }
        this.panel = panel;
        // reset percentage for progress
        percentDone = 0;

        // if given a filename, load maze from file
        // otherwise, show view and order maze from factory
//        if (filename != null) {
//
//        } else {
        // common case: generate maze with some algorithm
        assert null != factory : "GeneratingActivity.start(): factory must be present";

        // update the screen with the buffer graphics
//        panel.update();
        Log.v("gen_start","State gen: start factory order process");
        factory.order(this) ;
    }

    // for MazeFileReader part
//    /**
//     * Loads maze from file and returns a corresponding maze configuration.
//     * @param filename, not null
//     */
//    @RequiresApi(api = Build.VERSION_CODES.O)
//    private MazeConfiguration loadMazeConfigurationFromFile(String filename) {
//        // load maze from file
//        MazeFileReader mfr = new MazeFileReader(filename) ;
//        // obtain MazeConfiguration
//        return (MazeConfiguration) mfr.getMazeConfiguration();
//    }

    // private internal method to go to the PlayManuallyActivity without taking any parameter
    private void transitionToManualPrivate(){
        Intent intent = new Intent(this, PlayManuallyActivity.class);
        startActivity(intent);
    }

    // private internal method to go to the StateAnimationActivity without taking any parameter
    private void transitionToAnimePrivate(){
        Intent intent = new Intent(this, StateAnimationActivity.class);
        startActivity(intent);
    }

    // private internal method to go to the title without taking any parameter
    private void transitionToStartPrivate(){
        Intent intent = new Intent(this, AMazeActivity.class);
        startActivity(intent);
    }

    // handles the onClick method to the start menu for button BACK
    public void transitionToStartMenu(View view) {
//        progressHandler.removeCallbacks(runnable);
        Log.v("trans_to_start", "State gen: back to title menu");
        Intent intent = new Intent(this, AMazeActivity.class);
        startActivity(intent);
    }



    // debug
//    public void transitionToManual(View view) {
//        Intent intent = new Intent(this, PlayManuallyActivity.class);
//        startActivity(intent);
//    }
//
//    public void transitionToAnime(View view) {
//        Intent intent = new Intent(this, StateAnimationActivity.class);
//        startActivity(intent);
//    }
}
