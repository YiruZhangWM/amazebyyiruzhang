package edu.wm.cs.cs301.YiruZhang;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class LosingActivity extends AppCompatActivity {

    Button restartButton;
    TextView sorryText, distStatText;
    ImageView sorryImage;
    private int pathLen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_losing);
        restartButton = (Button) findViewById(R.id.restartButtonFinal2);
        sorryText = (TextView) findViewById(R.id.sorryText);
        distStatText = (TextView) findViewById(R.id.distFailText);
//        sorryImage = (ImageView) findViewById(R.id.loseImage);

        Intent intent = getIntent();
        if (intent.getIntExtra(StateAnimationActivity.PATH_LENGTH, -10) != -10){
            this.pathLen = intent.getIntExtra(StateAnimationActivity.PATH_LENGTH, -666);
        }
        else{
            this.pathLen = intent.getIntExtra(PlayManuallyActivity.PATH_LENGTH_MAN, -666);
        }

        distStatText.setText("Distance travelled: " + this.pathLen);
    }

    public void transitionToStartMenu(View view) {
        Log.v("fail_state", "State losing: restart the title page");
        Intent intent = new Intent(this, AMazeActivity.class);
        startActivity(intent);
    }
}
