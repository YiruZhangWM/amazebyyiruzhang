package edu.wm.cs.cs301.YiruZhang;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = new Intent(this, AMazeActivity.class);
        startActivity(intent);

//        // for testing
//        Intent intent_test = new Intent(this, LosingActivity.class);
//        startActivity(intent_test);

    }
}
