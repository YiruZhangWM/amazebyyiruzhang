package edu.wm.cs.cs301.YiruZhang;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import edu.wm.cs.cs301.YiruZhang.generation.CardinalDirection;
import edu.wm.cs.cs301.YiruZhang.generation.Cells;
import edu.wm.cs.cs301.YiruZhang.generation.MazeConfiguration;
import edu.wm.cs.cs301.YiruZhang.gui.Constants;
import edu.wm.cs.cs301.YiruZhang.gui.FirstPersonDrawer;
import edu.wm.cs.cs301.YiruZhang.gui.MapDrawer;
import edu.wm.cs.cs301.YiruZhang.gui.MazePanel;

public class PlayManuallyActivity extends AppCompatActivity {
    public static final String OPT_STATUS = "opt_status";
    public static final String RESTART_STATE = "restart_state";
    public static final String BASIC_OPERATION = "basic_operation";
    public static final String PATH_LENGTH_MAN = "path_length_man";
    public static final String BATTERY_LEFT_MAN = "battery_left_man";
    public static final String PLAY_MANUALLY = "play_manually";
    public static final float INIT_BATTERY = 2500f;
    Button backButton;
//    Button shortcutButton;
    TextView mapText, wallText, solText, batteryText;
    ImageView leftControl, rightControl, upControl, magButton, srkButton;
    ToggleButton fullMazeToggle, solutionToggle, allWallToggle;
    ProgressBar batteryBar;

    MazePanel mazePanel;
    FirstPersonDrawer firstPersonView;
    MapDrawer mapView;
    // current position and direction with regard to MazeConfiguration
    int px, py ; // current position on maze grid (x,y)
    int dx, dy;  // current direction

    int angle; // current viewing angle, east == 0 degrees
    int walkStep; // counter for intermediate steps within a single step forward or backward
    Cells seenCells; // a matrix with cells to memorize which cells are visible from the current point of view
    MazeConfiguration mazeConfig ;

    private boolean mapMode, showSolution, showAllWalls;

    private int pathLen;
    private float batteryLeft;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_manually);
        backButton = (Button) findViewById(R.id.restartManual);
        leftControl = (ImageView) findViewById(R.id.leftControl);
        rightControl = (ImageView) findViewById(R.id.rightControl);
        upControl = (ImageView) findViewById(R.id.upControl);
        fullMazeToggle = (ToggleButton) findViewById(R.id.fullMazeToggle);
        solutionToggle = (ToggleButton) findViewById(R.id.showSolutionToggle);
        allWallToggle = (ToggleButton) findViewById(R.id.allWallToggle);
        mazePanel = (MazePanel) findViewById(R.id.mazePanel);
        magButton = (ImageView) findViewById(R.id.magnifyButton);
        srkButton = (ImageView) findViewById(R.id.shrinkButton);
        mapText = (TextView) findViewById(R.id.mapText);
        solText = (TextView) findViewById(R.id.solText);
        wallText = (TextView) findViewById(R.id.wallText);
        batteryText = (TextView) findViewById(R.id.batteryTextPlay);
        batteryBar = (ProgressBar) findViewById(R.id.progressBar);
        batteryBar.setProgress(100);

        backButton.bringToFront();
        fullMazeToggle.bringToFront();
        solutionToggle.bringToFront();
        allWallToggle.bringToFront();
        mapText.bringToFront();
        solText.bringToFront();
        wallText.bringToFront();
        leftControl.bringToFront();
        rightControl.bringToFront();
        upControl.bringToFront();
        magButton.bringToFront();
        srkButton.bringToFront();

        mazeConfig = GeneratingActivity.config;
        Log.v(PLAY_MANUALLY, String.format("Graphics playManual: (onCreate) mazeConfig == null is: <%s>", GeneratingActivity.config == null));
        seenCells = new Cells(mazeConfig.getWidth()+1,mazeConfig.getHeight()+1) ;
        // set the current position and direction consistently with the viewing direction
//        setPositionDirectionViewingDirection();
        walkStep = 0; // counts incremental steps during move/rotate operation
        // initialize the settings
        mapMode = false; showSolution = false; showAllWalls = false;
        // initialize the battery and pathLen setting
        batteryLeft = INIT_BATTERY; pathLen = 0;
        batteryText.setText(String.format("Remaining Battery: %s", batteryLeft));

        int[] start = mazeConfig.getStartingPosition();
        setCurrentPosition(start[0], start[1]);
        Log.v(PLAY_MANUALLY, String.format("Op: (onCreate) initial position is (%s, %s)", px, py));
        angle = 0;  // initial direction is always east
        setDirectionToMatchCurrentAngle();
        // initial direction is east, check this for sanity:
        Log.v(PLAY_MANUALLY, String.format("Op: (onCreate) initial direction is (%s, %s)", dx, dy));

        fullMazeToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked){
                    Toast.makeText(PlayManuallyActivity.this, "Displaying the map", Toast.LENGTH_SHORT).show();
                    magButton.setVisibility(View.VISIBLE);
                    srkButton.setVisibility(View.VISIBLE);
                }
                else {
                    magButton.setVisibility(View.INVISIBLE);
                    srkButton.setVisibility(View.INVISIBLE);
                }
                Log.v(OPT_STATUS, String.format("Op: mapMode is <%s>", mapMode));
                keyDown("toggle_show_map");
            }
        });

        solutionToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked){
                    if (!mapMode){
                        Toast.makeText(PlayManuallyActivity.this, "Toggle the [display full maze] to show solution", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(PlayManuallyActivity.this, "Showing the solution", Toast.LENGTH_SHORT).show();
                    }

                }
                else{
                    if (mapMode) {
                        Toast.makeText(PlayManuallyActivity.this, "Hiding the solution", Toast.LENGTH_SHORT).show();
                    }
                }
                Log.v(OPT_STATUS, String.format("Op: showSolution is <%s>", showSolution));
                keyDown("toggle_show_solution");
            }
        });

        allWallToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked){
                    if (!mapMode){
                        Toast.makeText(PlayManuallyActivity.this, "Toggle the [display full maze] to see all walls", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(PlayManuallyActivity.this, "Showing all walls", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    if (mapMode) {
                        Toast.makeText(PlayManuallyActivity.this, "Hiding unseen walls", Toast.LENGTH_SHORT).show();
                    }
                }
                Log.v(OPT_STATUS, String.format("Op: showAllWalls is <%s>", showAllWalls));
                keyDown("toggle_show_walls");
            }
        });

        Log.v("test_play","1. mazePanel.setactivity(this)");
        mazePanel.setActivity(this);
//        mazePanel.playActivityStart();

        Log.v(OPT_STATUS, String.format("Op: OnCreate(): showAllWalls is <%s>", this.mapMode));
    }

    public void transitionToStartMenu(View view) {
        Log.v(RESTART_STATE, "State playManual: restart for user's trigger");
        Intent intent = new Intent(this, AMazeActivity.class);
        startActivity(intent);
    }

//    public void transitionToFinal(View view) {
//        Log.v(RESTART_STATE, "State playManual: shortcut to final activity for user's trigger");
//        Intent intent = new Intent(this, FinishActivity.class);
//        intent.putExtra(PATH_LENGTH_MAN, this.pathLen);
//        intent.putExtra(BATTERY_LEFT_MAN, this.batteryLeft);
//        startActivity(intent);
//    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void play_button_up(View view) {
        Log.v(BASIC_OPERATION, "State playManual: basic op - FORWARD");
        keyDown("forward");
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void play_button_left(View view) {
        Log.v(BASIC_OPERATION, "State playManual: basic op - LEFT");
        keyDown("left");
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void play_button_right(View view) {
        Log.v(BASIC_OPERATION, "State playManual: basic op - RIGHT");
        keyDown("right");
    }

    //////////////////////////////for graphics/////////////////////////

    /**
     * Initializes the drawer for the first person view
     * and the map view and then draws the initial screen
     * for this state.
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void startDrawer() {
        firstPersonView = new FirstPersonDrawer(Constants.VIEW_WIDTH,
                Constants.VIEW_HEIGHT, Constants.MAP_UNIT,
                Constants.STEP_SIZE, seenCells, mazeConfig.getRootnode()) ;
        mapView = new MapDrawer(seenCells, 35, mazeConfig) ;
        Log.v(PLAY_MANUALLY,String.format("Graphics playManual: seenCells == null is: <%s>", seenCells == null));
        // draw the initial screen for this state
        draw();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    protected void draw(){
        if (null == this.mazePanel){
            Log.v("play_manually_activity","State man_act: panel is null");
        }
        Log.v("angle_report",String.format("Angle is <%s>", angle));
        // draw the first person view and the map view if wanted
        firstPersonView.draw(mazePanel, px, py, walkStep, angle) ;
        if (mapMode) {
            mapView.draw(mazePanel, px, py, angle, walkStep,
                    showAllWalls, showSolution) ;
        }
        // update the screen with the buffer graphics
        mazePanel.update() ;
    }

    private void adjustMapScale(boolean increment) {
        if (increment) {
            mapView.incrementMapScale() ;
        }
        else {
            mapView.decrementMapScale() ;
        }
    }

    /////////////////////////////for operation///////////////////////////

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void keyDown(String input){
        switch (input){
            case "forward": // move forward
                if (this.batteryLeft < 4){
                    transitionToLosingPrivate();
                }
                else {
                    walk(1);
                    // check termination, did we leave the maze?
                    if (isOutside(px, py)) {
                        Log.v(PLAY_MANUALLY, "State play: you are OUTSIDE, transition to final (PRIVATE)");
                        transitionToFinalPrivate();
                    }
                    batteryLeft = batteryLeft < 4 ? 0 : batteryLeft - 4;
                    this.pathLen += 1;
                    batteryBar.setProgress((int)(100*batteryLeft / INIT_BATTERY));
                    batteryText.setText(String.format("Remaining Battery: %s", batteryLeft));
                }
                break;
            case "left": // turn left
                if (this.batteryLeft < 3){
                    transitionToLosingPrivate();
                }
                else {
                    rotate(1);
                    batteryLeft = batteryLeft < 3 ? 0 : batteryLeft - 3;
//                    this.batteryLeft -= 3;
                    batteryBar.setProgress((int)(100*batteryLeft / INIT_BATTERY));
                    batteryText.setText(String.format("Remaining Battery: %s", batteryLeft));
                }
                break;
            case "right": // turn right
                if (this.batteryLeft < 3){
                    transitionToLosingPrivate();
                }
                else {
                    rotate(-1);
                    batteryLeft = batteryLeft < 3 ? 0 : batteryLeft - 3;
                    batteryBar.setProgress((int)(100*batteryLeft / INIT_BATTERY));
                    batteryText.setText(String.format("Remaining Battery: %s", batteryLeft));
                }
                break;
            case "toggle_show_walls": // enable/disable show walls mode
                showAllWalls = !showAllWalls;
                Log.v(PLAY_MANUALLY, String.format("Op: keyDown() show_wall is now: <%s>", this.showAllWalls));
                draw();
                break;
            case "toggle_show_map": // enable/disable show whole map mode
                Log.v(PLAY_MANUALLY, String.format("Op: [prev] keyDown() show_map is now: <%s>", this.mapMode));
                mapMode = !mapMode;
                Log.v(PLAY_MANUALLY, String.format("Op: keyDown() show_map is now: <%s>", this.mapMode));
                draw();
                break;
            case "toggle_show_solution": // enable/disable show solution map mode
                showSolution = !showSolution;
                Log.v(PLAY_MANUALLY, String.format("Op: keyDown() show_solution is now: <%s>", this.showSolution));
                draw();
                break;
            case "zoom_in": // zoom into map, currently not used
                adjustMapScale(true);
                draw() ;
                break ;
            case "zoom_out": // zoom out of map, currently not used
                adjustMapScale(false);
                draw() ;
                break ;
            default:
                Log.v(PLAY_MANUALLY, String.format("Op: keyDown() unrecognized command: <%s>", input));
                break;

        }
    }

    /**
     * Performs a rotation with 4 intermediate views,
     * updates the screen and the internal direction
     * @param dir for current direction, values are either 1 or -1
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    synchronized private void rotate(int dir) {
        final int originalAngle = angle;
        final int steps = 4;

        for (int i = 0; i != steps; i++) {
            // add 1/4 of 90 degrees per step
            // if dir is -1 then subtract instead of addition
            angle = originalAngle + dir*(90*(i+1))/steps;
            angle = (angle+1800) % 360;
            // draw method is called and uses angle field for direction
            // information.
            slowedDownRedraw();
        }
        // update maze direction only after intermediate steps are done
        // because choice of direction values are more limited.
        setDirectionToMatchCurrentAngle();
    }

    /**
     * Moves in the given direction with 4 intermediate steps,
     * updates the screen and the internal position
     * @param dir, only possible values are 1 (forward) and -1 (backward)
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    synchronized private void walk(int dir) {
//        Log.v(PLAY_MANUALLY, String.format("Op: (px, py) is (%s, %s)", px, py));
//        Log.v(PLAY_MANUALLY, String.format("Op: (dx, dy) is (%s, %s)", dx, dy));

        // check if there is a wall in the way
        if (!checkMove(dir))
            return;
        // walkStep is a parameter of FirstPersonDrawer.draw()
        // it is used there for scaling steps
        // so walkStep is implicitly used in slowedDownRedraw
        // which triggers the draw operation in
        // FirstPersonDrawer and MapDrawer
        for (int step = 0; step != 4; step++) {
            walkStep += dir;
            slowedDownRedraw();
        }
        setCurrentPosition(px + dir*dx, py + dir*dy) ;
        walkStep = 0; // reset counter for next time
        //logPosition(); // debugging
    }

    /**
     * Checks if the given position is outside the maze
     * @param x coordinate of position
     * @param y coordinate of position
     * @return true if position is outside, false otherwise
     */
    private boolean isOutside(int x, int y) {
        return !mazeConfig.isValidPosition(x, y) ;
    }
    /**
     * Helper method for walk()
     * @param dir
     * @return true if there is no wall in this direction
     */
    protected boolean checkMove(int dir) {
        CardinalDirection cd = null;
        Log.v(PLAY_MANUALLY, String.format("Op: checkMove() current direction is: <%s>", getCurrentDirection()));
        switch (dir) {
            case 1: // forward
                cd = getCurrentDirection();
                break;
            case -1: // backward
                cd = getCurrentDirection().oppositeDirection();
                break;
            default:
                throw new RuntimeException("Unexpected direction value: " + dir);
        }
        return !mazeConfig.hasWall(px, py, cd);
    }
    /**
     * Draws and waits. Used to obtain a smooth appearance for rotate and move operations
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void slowedDownRedraw() {
        draw() ;
        try {
            Thread.sleep(10);
        } catch (Exception e) {
            // may happen if thread is interrupted
            // no reason to do anything about it, ignore exception
        }
    }

    /**
     * Internal method to set the current position, the direction
     * and the viewing direction to values consistent with the
     * given maze.
     */
    private void setPositionDirectionViewingDirection() {
        // obtain starting position
        int[] start = mazeConfig.getStartingPosition() ;
        setCurrentPosition(start[0],start[1]) ;
        // set current view direction and angle
        angle = 0; // angle matches with east direction,
        // hidden consistency constraint!
        setDirectionToMatchCurrentAngle();
        // initial direction is east, check this for sanity:
        assert(dx == 1);
        assert(dy == 0);
    }

    /////////////////////////////////set methods//////////////////////////
    public void setMazeConfiguration(MazeConfiguration config) {
        mazeConfig = config;
    }
    protected void setCurrentPosition(int x, int y) {
        px = x ;
        py = y ;
    }
    private void setCurrentDirection(int x, int y) {
        dx = x ;
        dy = y ;
    }
    private void setDirectionToMatchCurrentAngle() {
        setCurrentDirection((int) Math.cos(radify(angle)), (int) Math.sin(radify(angle))) ;
    }
    final double radify(int x) {
        return x*Math.PI/180;
    }
    ////////////////////////////// get methods ///////////////////////////////////////////////////////////////
    protected int[] getCurrentPosition() {
        int[] result = new int[2];
        result[0] = px;
        result[1] = py;
        return result;
    }
    protected CardinalDirection getCurrentDirection() {
        return CardinalDirection.getDirection(dx, dy);
    }
    boolean isInMapMode() {
        return mapMode ;
    }
    boolean isInShowWallsMode() {
        return showAllWalls ;
    }
    boolean isInShowSolutionMode() {
        return showSolution ;
    }
    public MazeConfiguration getMazeConfiguration() {
        return mazeConfig ;
    }

    /////////////////////////private transition methods/////////////////
    private void transitionToFinalPrivate(){
        Intent intent = new Intent(this, FinishActivity.class);
        intent.putExtra(PATH_LENGTH_MAN, this.pathLen);
        intent.putExtra(BATTERY_LEFT_MAN, this.batteryLeft);
        startActivity(intent);
    }

    private void transitionToLosingPrivate(){
        Log.v("play_toLosing","toFail: transition to losing stage");
        Intent intent = new Intent(this, LosingActivity.class);
        intent.putExtra(PATH_LENGTH_MAN, this.pathLen);
        startActivity(intent);
    }

    // onClick methods for the magnify button and shrink button
    // main function: show up when show_map is on, disappear when show_map is off
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void onClickMagnifyButton(View view) {
        Log.v(PLAY_MANUALLY, "Op: magnify button clicked");
        keyDown("zoom_in");
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void onClickShrinkButton(View view) {
        Log.v(PLAY_MANUALLY, "Op: shrink button clicked");
        keyDown("zoom_out");
    }
}
