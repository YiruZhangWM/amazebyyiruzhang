package edu.wm.cs.cs301.YiruZhang;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.ToggleButton;

public class StateAnimationActivity extends AppCompatActivity {
    public static final String OPT_STATUS_ANIME = "opt_status_anime";
    public static final String RESTART_STATUS_ANIME = "restart_status_anime";
    public static final String PATH_LENGTH = "path_length";
    public static final String BATTERY_LEFT = "battery_left";
    Button backButton, shortcutButton;
    ToggleButton fullMazeToggle, solutionToggle, allWallToggle;
    boolean showFullMaze = false, showSolution = false, showAllWalls = false;
    private int pathLen = 0;
    private float batteryLeft = 0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_state_animation);
        backButton = (Button) findViewById(R.id.restartAnime);
        shortcutButton = (Button) findViewById(R.id.shortcutAnime);
        fullMazeToggle = (ToggleButton) findViewById(R.id.fullMazeToggleAnime);
        solutionToggle = (ToggleButton) findViewById(R.id.showSolAnime);
        allWallToggle = (ToggleButton) findViewById(R.id.allWallAnime);

        fullMazeToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked){
                    showFullMaze = true;
                    Toast.makeText(StateAnimationActivity.this, "Displaying the whole maze", Toast.LENGTH_SHORT).show();
                    Log.v(OPT_STATUS_ANIME, "State animation: showFullMaze is ON");
                }
                else {
                    showFullMaze = false;
                    Toast.makeText(StateAnimationActivity.this, "Hiding unreached section of the maze", Toast.LENGTH_SHORT).show();
                    Log.v(OPT_STATUS_ANIME, "State animation: showFullMaze is OFF");
                }
            }
        });

        solutionToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked){
                    showSolution = true;
                    Toast.makeText(StateAnimationActivity.this, "Showing the solution", Toast.LENGTH_SHORT).show();
                    Log.v(OPT_STATUS_ANIME, "State animation: showSolution is ON");
                }
                else{
                    showSolution = false;
                    Toast.makeText(StateAnimationActivity.this, "Hiding the solution", Toast.LENGTH_SHORT).show();
                    Log.v(OPT_STATUS_ANIME, "State animation: showSolotion is OFF");
                }
            }
        });

        allWallToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked){
                    showAllWalls = true;
                    Toast.makeText(StateAnimationActivity.this, "Showing all walls", Toast.LENGTH_SHORT).show();
                    Log.v(OPT_STATUS_ANIME, "State animation: showAllWalls is ON");
                }
                else{
                    showAllWalls = false;
                    Toast.makeText(StateAnimationActivity.this, "Hiding unseen walls", Toast.LENGTH_SHORT).show();
                    Log.v(OPT_STATUS_ANIME, "State animation: showAllWalls is OFF");
                }
            }
        });
    }

    public void transitionToStartMenu(View view) {
        Log.v(RESTART_STATUS_ANIME, "State animation: restart to state title by user's trigger");
        Intent intent = new Intent(this, AMazeActivity.class);
        startActivity(intent);
    }

    public void transitionToFinal(View view) {
        Log.v(RESTART_STATUS_ANIME, "State animation: shortcut to state final by user's trigger");
        Intent intent = new Intent(this, FinishActivity.class);
        intent.putExtra(PATH_LENGTH, this.pathLen);
        intent.putExtra(BATTERY_LEFT, this.batteryLeft);
        startActivity(intent);
    }
}
