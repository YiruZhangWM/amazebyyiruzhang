package edu.wm.cs.cs301.YiruZhang.generation;

//import java.util.ArrayList;

/**
 * Cells Coordinate class encapsulates the coordinate of each cell
 * @author Yiru Zhang
 *
 */
public class CellsCoord {

	private int x;
	private int y;
	
	public CellsCoord(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getX() {
		return this.x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public int getY() {
		return this.y;
	}
	
	/**
	 * checks if two CellsCoord are the same
	 */
	public boolean equals(CellsCoord cc1) {
//		assert (cc1 != null) : "cc1 cannot be null!";
		if (cc1 == null)
			return false;
		boolean isEqual = (cc1.getX() == this.x) && (cc1.getY() == this.y);
		return isEqual;
	}

}
