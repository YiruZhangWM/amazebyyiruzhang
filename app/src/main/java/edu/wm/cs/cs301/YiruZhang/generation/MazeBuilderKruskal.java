package edu.wm.cs.cs301.YiruZhang.generation;

import java.util.*;


/**
 * Intuitions for implementing Kruskal's algo.: 
 * 1. put all edges in a bag;
 * 2. pull out the edge with the lowest weight: 
 * 		if it connects 2 disjoint trees, join the trees;
 * 		otherwise, throw away the edge;
 * 3. repeat the process until no edges is left.
 * 
 * Reference: (from the project mannual)
 * http://weblog.jamisbuck.org/2011/1/3/maze-generation-kruskal-s-algorithm.html
 * @author Yiru Zhang
 *
 */
public class MazeBuilderKruskal extends MazeBuilder implements Runnable {
	
	protected ArrayList<ArrayList<ArrayList<CellsCoord> > > mazeSets;
	protected ArrayList<Wall> edgesQ = new ArrayList<Wall>(); 
	
	public MazeBuilderKruskal() {
		super();
		System.out.println("MazeBuilderKruskal uses Kruskal's algorithm to generate maze.");
	}
	
	public MazeBuilderKruskal(boolean det) {
		super(det);
		System.out.println("MazeBuilderKruskal uses Kruskal's algorithm to generate deterministic maze.");
	}
	
	/**
	 * This method generates pathways into the maze using Kruskal's algorithm. 
	 * mazeSets is a 3D array that stores the grid and each 
	 * corresponding sets of cells:
	 * the first 2 dimensions describe the coordinates and the 3rd dimension 
	 * describe the cell's set; 
	 * edgesQ is a queue that stores all the edges (walls) 
	 */
	protected void generatePathways() {
		
		//initialize the mazeSets to a 3D array
		mazeSets = new ArrayList<ArrayList<ArrayList<CellsCoord> > >();
		for (int w = 0; w < width; ++w) {
			mazeSets.add(new ArrayList<ArrayList<CellsCoord> >());
			for (int h = 0; h < height; ++h) {
				mazeSets.get(w).add(new ArrayList<CellsCoord>());
			}
		}
		
//		System.out.println("FOR DEBUG PURPOSE: PRINTING THE MAZE SIZE");
//		System.out.println("mazeSets.size() is: " + mazeSets.size());
//		System.out.println("mazeSets.get(0).size() is: " + mazeSets.get(0).size());
//		System.out.println("mazeSets.get(0).get(0).size() is: " + mazeSets.get(0).get(0).size());
		
		// 1. initialize the mazeSets by adding the coordinates of each cell into 
		// the 3D array; 
		// 2. initialize the edges queue by adding all the edges into the wall
		for (int w = 0; w < width; ++w) {
			for (int h = 0; h < height; ++h) {
				CellsCoord temp = new CellsCoord(w, h);
				mazeSets.get(w).get(h).add(temp);		// 1
				if (w > 0) {	// 2
					edgesQ.add(new Wall(w, h, CardinalDirection.West));
				}
				if (h > 0) {
					edgesQ.add(new Wall(w, h, CardinalDirection.North));
				}
			}
		}
		
		// debug
//		System.out.println("FOR DEBUG PURPOSE: PRINTING THE MAZE CONFIG");
//		printMaze();
		
		// repeat until no edge is left in the queue
		while (!edgesQ.isEmpty()) {
			// randomly pick an edge from the queue
			Wall cur = getRandWallFromQ(edgesQ);
			
			// obtain the current coordinates of the current cell (x, y) 
			// and the other cell that the edge connects (nx, ny)
			int x = cur.getX();
			int y = cur.getY();
			int nx = cur.getNeighborX();
			int ny = cur.getNeighborY();
//			System.out.println("FROM KRUSKAL genPathways()");
//			System.out.println("cur is: (" + cur.getX() + ", " + cur.getY() + ", " + cur.getDirection() + ")");
//			System.out.println("(x, y) is: (" + x + ", " + y + ")");
//			System.out.println("(nx, ny) is: (" + nx + ", " + ny + ")");
			
			ArrayList<CellsCoord> set1 = mazeSets.get(x).get(y);	//(x, y)
			ArrayList<CellsCoord> set2 = mazeSets.get(nx).get(ny);	//(nx, ny)
			
//			System.out.println("set 1 is: ");
//			printSet(x, y);
//			System.out.println("set 2 is: ");
//			printSet(nx, ny);
//			System.out.println();
			
			// if set1 and set2 are disjoint, join the two sets
			if (!isConnected(set1, set2.get(0))) {
				cells.deleteWall(cur);
				connect(set1, set2);
//				System.out.println("connect(set1, set2) finished");
//				System.out.println("set 1 is: ");
//				printSet(x, y);
//				System.out.println("set 2 is: ");
//				printSet(nx, ny);
//				System.out.println();
			}
			
		}
		//System.out.println("TOTAL FINISH WHILE");
	}
	
	/**
	 * Randomly pick an edge from the edges queue
	 * @param edges queue
	 * @return a random edge (wall) from the queue
	 */
	protected Wall getRandWallFromQ(ArrayList<Wall> edges) {
		// edges should not be null
		assert (edges != null) : "getRandWallFromQ(): edges cannot be null!";
		// edges should have at least 1 element
		assert (edges.size() != 0) : "getRandWallFromQ(): edges.size cannot be 0!";
		return edges.remove(random.nextIntWithinInterval(0, edges.size() - 1));
	}
	
	/**
	 * See if one CellsCoord object, cc, is already inside a set
	 * @param set
	 * @return true if a set contains a certain element of CellsCoord
	 */
	protected boolean isConnected(ArrayList<CellsCoord> set, CellsCoord cc) {
		// set should not be null
		assert (set != null) : "isConnected(): set cannot be null!";
		for (int i = 0; i < set.size(); ++i) {
			if (set.get(i).equals(cc)) {
				//System.out.println("isConnected! Reject!");
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Replicate the elements in the other set and append it to its set to indicate
	 * the sharing of identity
	 * @param set1
	 * @param set2
	 */
	protected void connect(ArrayList<CellsCoord> set1, ArrayList<CellsCoord> set2) {
		// append all the elements in set2 to set1
		for (int i = 0; i < set2.size(); ++i) {
			set1.add(set2.get(i));
		}
		// update other sets that shares the array with set1
		copySets(set1, set1);
		// append all the elements in set1 to set2
		copySets(set2, set1);
	}
	
	/**
	 * Update all the CellsCoord in the 'set' to be the same as the elements 
	 * in 'copy'
	 * @param set includes elements to be copied
	 * @param copy is the target set
	 */
	protected void copySets(ArrayList<CellsCoord> set, ArrayList<CellsCoord> copy) {
		CellsCoord cc;
		ArrayList<CellsCoord> ccSet;
		for (int i = 0; i < set.size(); ++i) {
			for (int j = 0; j < copy.size(); ++j) {
				cc = set.get(i);
				ccSet = this.mazeSets.get(cc.getX()).get(cc.getY());
				if (!ccSet.contains(copy.get(j))) {
					ccSet.add(copy.get(j));
				}
			}
		}
	}
	
	//debug
//	protected void printSet(int x, int y) {
//		for (int i = 0; i < mazeSets.get(x).get(y).size(); ++i) {
//			System.out.print("(" + mazeSets.get(x).get(y).get(i).getX() + 
//							 ", " + mazeSets.get(x).get(y).get(i).getY() + ") ");
//		}
//		System.out.println();
//	}
	
//	protected void printMaze() {
//		// length matrix
//		for (int i = 0; i < width; ++i) {
//			for (int j = 0; j < height; ++j) {
//				System.out.print(mazeSets.get(i).get(j).size() + " ");
//			}
//			System.out.println();
//		}
		
		// individual arrays
//		for (int i = 0; i < width; ++i) {
//			for (int j = 0; j < height; ++j) {
//				for (int k = 0; k < mazeSets.get(i).get(j).size(); ++k) {
//					System.out.print(mazeSets.get(i).get(j).get(k) + " ");
//				}
//				System.out.println();
//			}
//		}
}
