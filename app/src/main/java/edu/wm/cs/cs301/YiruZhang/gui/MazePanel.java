package edu.wm.cs.cs301.YiruZhang.gui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import edu.wm.cs.cs301.YiruZhang.GeneratingActivity;
import edu.wm.cs.cs301.YiruZhang.PlayManuallyActivity;

/**
 * Handles maze graphics.
 */
public class MazePanel extends View {
    // TODO: please check 
    // https://developer.android.com/training/custom-views/create-view
    // https://developer.android.com/training/custom-views/custom-drawing
    // on how to implement your own View class
    //
    final double n = 3.6; // sets the magnifying coefficient for graphics

    private Context mViewContext;
    private AttributeSet mViewAttrSet;

    private int mColor;
    private Paint mPaint;
    private Canvas mCanvas;
    private Path mPath;
    private Bitmap mBitmap;
    private int width;
    private int height;

    private Rect mRect = new Rect();

    PlayManuallyActivity manuallyActivity;
    GeneratingActivity genActivity;


    /**
     * Constructor with one context parameter.
     * @param context
     */
    public MazePanel(Context context) {
        // call super class constructor as necessary
        // TODO: initialize instance variables as necessary
        super(context);
        this.mViewContext = context;
        init(null);
    }
    /**
     * Constructor with two parameters: context and attributes.
     * @param context
     * @param app
     */
    public MazePanel(Context context, AttributeSet app) {
        // call super class constructor as necessary
        // TODO: initialize instance variables as necessary
        super(context, app);
        this.mViewContext = context;
        this.mViewAttrSet = app;
        init(app);
    }

    private void init(@Nullable AttributeSet set){
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPath = new Path();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.width = w;
        this.height = h;

        Log.v("maze_panel_size_change","initializing mBitmap and mCanvas");
        mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);

        Log.v("test_play","2. man_acti.startDrawer");
        manuallyActivity.startDrawer();
    }

    /**
     * Draws given canvas.
     * @param canvas
     */
    @Override
	public void onDraw(Canvas canvas) {
	    // TODO: draw bitmap
        super.onDraw(canvas);
        canvas.drawBitmap(mBitmap, 0, 0, null);
    }
    
    /**
     * Measures the view and its content to determine the measured width and the measured height.
     * @param width
     * @param height
     */
    @Override
    public void onMeasure(int width, int height) {
	// as described for superclass method
        super.onMeasure(width, height);
    }
    
    /**
     * Updates maze graphics.
     */
    public void update() {
	    //TODO: update maze graphics
        invalidate();
    }
    
    /**
     * Takes in color string, sets paint color to corresponding color. 
     * @param c color string
     */
    public void setColor(String c) {
	    // TODO: same as setColor(int) but for string parameters
        mPaint.setColor(Color.parseColor(c));
    }
    
    /**
     * Sets paint object color attribute to given color.
     * @param color
     */
    public void setColor(int color) {
	// TODO: set the current color
        mPaint.setColor(color);
    }
    
    /**
     * Takes in color integer values [0-255], returns corresponding color-int value. 
     * red green blue are color values
     */
    public static int getColorEncoding(int red, int green, int blue) {
	    // TODO: provide rgb color encoding
        return Color.rgb(red, green, blue);
    }

    /**
     * An overloaded version of the getColorEncoding method. This method
     * allows the use of opacity (alpha)
     */
    public static int getColorEncoding(int alpha, int red, int green, int blue) {
        return Color.argb(alpha, red, green, blue);
    }
    
    /**
     * Returns the RGB value representing the current color. 
     * @return integer RGB value
     */
    public int getColor() {
	// TODO return the current color setting
        return this.mColor;
    }
    
    /**
     * Takes in rectangle params, fills rectangle in canvas based on these. 
     * @param x
     * @param y
     * @param width
     * @param height
     */
    public void fillRect(int x, int y, int width, int height) {
        if (this.mCanvas == null){
            Log.v("maze_panel_fillrect","ERROR: mCanvas is null");
        }
        mRect.set((int)(n*x), (int)(n*y), (int)(n*(x + width)), (int)(n*(y + height)));
        mCanvas.drawRect(mRect, mPaint);
    }
    
    /**
     * Takes in polygon params, fills polygon in canvas based on these. 
     * Paint is always that for corn.
     * @param xPoints
     * @param yPoints
     * @param nPoints
     */
    public void fillPolygon(int[] xPoints, int[] yPoints, int nPoints){
	    // translate the points into a path
	    // draw a path on the canvas
        Path wallpath = new Path();
        wallpath.reset();
        wallpath.moveTo((int)(n*xPoints[0]), (int)(n*yPoints[0]));
        for (int i = 1; i < nPoints; ++i){
            wallpath.lineTo((int)(n*xPoints[i]), (int)(n*yPoints[i]));
        }
        wallpath.lineTo((int)(n*xPoints[0]), (int)(n*yPoints[0]));
        mCanvas.drawPath(wallpath, mPaint);
    }
    
    /**
     * Takes in line params, draws line in canvas based on these.
     * Thicken the lines by drawing a total of 7 lines.
     */
    public void drawLine(int x1, int y1, int x2, int y2) {
        mCanvas.drawLine((int)(n*x1), (int)(n*y1), (int)(n*x2), (int)(n*y2), mPaint);
        mCanvas.drawLine((int)(n*x1 + 1), (int)(n*y1 + 1), (int)(n*x2 + 1), (int)(n*y2 + 1), mPaint);
        mCanvas.drawLine((int)(n*x1 + 2), (int)(n*y1 + 2), (int)(n*x2 + 2), (int)(n*y2 + 2), mPaint);
        mCanvas.drawLine((int)(n*x1 + 3), (int)(n*y1 + 3), (int)(n*x2 + 3), (int)(n*y2 + 3), mPaint);
        mCanvas.drawLine((int)(n*x1 - 1), (int)(n*y1 - 1), (int)(n*x2 - 1), (int)(n*y2 - 1), mPaint);
        mCanvas.drawLine((int)(n*x1 - 2), (int)(n*y1 - 2), (int)(n*x2 - 2), (int)(n*y2 - 2), mPaint);
        mCanvas.drawLine((int)(n*x1 - 3), (int)(n*y1 - 3), (int)(n*x2 - 3), (int)(n*y2 - 3), mPaint);
    }
    
    /**
     * Takes in oval params, fills oval in canvas based on these. 
     * @param x
     * @param y
     * @param width
     * @param height
     */
    public void fillOval(int x, int y, int width, int height) {
	// TODO: draw an oval on the canvas
        mCanvas.drawLine((int)(n*x), (int)(n*y), (int)(n*(x + width)), (int)(n*(y + height)), mPaint);
    }

    public void setGenActivity(GeneratingActivity acti){
        this.genActivity = acti;
    }
    public void genActivityStart(){
        if (genActivity == null){
            Log.v("panel_gen_start","ERROR: gen activity is null, cannot start");
            System.exit(0);
        }
        genActivity.start(this);
    }
    public void setActivity(PlayManuallyActivity activity){
        this.manuallyActivity = activity;
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void playActivityStart(){
        if (manuallyActivity == null){
            Log.v("panel_play_start","ERROR: play activity is null, cannot start");
            System.exit(0);
        }
        manuallyActivity.startDrawer();
    }

}
